import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity'
import {Customer} from '../customer.model';

export enum CustomerActionTypes{
    LOAD_CUSTOMERS = "[Cusomer] Load customers",
    LOAD_CUSTOMERS_SUCCESS = "[Cusomer] Load customers success",
    LOAD_CUSTOMERS_FAIL = "[Cusomer] Load customers fail",

    LOAD_CUSTOMER = "[Cusomer] Load customer",
    LOAD_CUSTOMER_SUCCESS = "[Cusomer] Load customer success",
    LOAD_CUSTOMER_FAIL = "[Cusomer] Load customer fail",

    CREATE_CUSTOMER = "[Cusomer] Create customer",
    CREATE_CUSTOMER_SUCCESS = "[Cusomer] Create customer success",
    CREATE_CUSTOMER_FAIL = "[Cusomer] Create customer fail",

    UPDATE_CUSTOMER = "[Cusomer] Update customer",
    UPDATE_CUSTOMER_SUCCESS = "[Cusomer] Update customer success",
    UPDATE_CUSTOMER_FAIL = "[Cusomer] Update customer fail",

    DELETE_CUSTOMER = "[Cusomer] Delete customer",
    DELETE_CUSTOMER_SUCCESS = "[Cusomer] Delete customer success",
    DELETE_CUSTOMER_FAIL = "[Cusomer] Delete customer fail"


}

export class LoadCustomers implements Action{
    readonly type = CustomerActionTypes.LOAD_CUSTOMERS;   
}

export class LoadCustomersSuccess implements Action{
    readonly type = CustomerActionTypes.LOAD_CUSTOMERS_SUCCESS;   
    constructor(public payload: Customer[]){}
}

export class LoadCustomersFail implements Action{
    readonly type = CustomerActionTypes.LOAD_CUSTOMERS_FAIL; 
    constructor(public payload: string){}
  
}
// load

export class LoadCustomer implements Action{
    readonly type = CustomerActionTypes.LOAD_CUSTOMER;   
    constructor(public payload: number){}
}

export class LoadCustomerSuccess implements Action{
    readonly type = CustomerActionTypes.LOAD_CUSTOMER_SUCCESS;   
    constructor(public payload: Customer){}
}

export class LoadCustomerFail implements Action{
    readonly type = CustomerActionTypes.LOAD_CUSTOMER_FAIL; 
    constructor(public payload: string){}
  
}

// Create
export class CreateCustomer implements Action{
    readonly type = CustomerActionTypes.CREATE_CUSTOMER;   
    constructor(public payload: Customer){}
}

export class CreateCustomerSuccess implements Action{
    readonly type = CustomerActionTypes.CREATE_CUSTOMER_SUCCESS;   
    constructor(public payload: Customer){}
}

export class CreateCustomerFail implements Action{
    readonly type = CustomerActionTypes.CREATE_CUSTOMER_FAIL; 
    constructor(public payload: string){}
  
}


// Update
export class UpdateCustomer implements Action{
    readonly type = CustomerActionTypes.UPDATE_CUSTOMER;   
    constructor(public payload: Customer){}
}

export class UpdateCustomerSuccess implements Action{
    readonly type = CustomerActionTypes.UPDATE_CUSTOMER_SUCCESS;   
    constructor(public payload: Update<Customer>){}
}

export class UpdateCustomerFail implements Action{
    readonly type = CustomerActionTypes.UPDATE_CUSTOMER_FAIL; 
    constructor(public payload: string){}
  
}


// Delete
export class DeleteCustomer implements Action{
    readonly type = CustomerActionTypes.DELETE_CUSTOMER;   
    constructor(public payload: number){}
}

export class DeleteCustomerSuccess implements Action{
    readonly type = CustomerActionTypes.DELETE_CUSTOMER_SUCCESS;   
    constructor(public payload: number){}
}

export class DeleteCustomerFail implements Action{
    readonly type = CustomerActionTypes.DELETE_CUSTOMER_FAIL; 
    constructor(public payload: string){}
  
}

export type Action = 
LoadCustomers | 
LoadCustomersFail | 
LoadCustomersSuccess |

LoadCustomer | 
LoadCustomerFail | 
LoadCustomerSuccess |

CreateCustomer | 
CreateCustomerFail | 
CreateCustomerSuccess |

UpdateCustomer | 
UpdateCustomerFail | 
UpdateCustomerSuccess |

DeleteCustomer | 
DeleteCustomerFail | 
DeleteCustomerSuccess

;